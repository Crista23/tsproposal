% !TEX root = ts2015-proposal.tex
\vspace{-.5\baselineskip}
\section{Proposed Approach}
\label{section:approach}
\vspace{-.5\baselineskip}

Following Zhao et al~\cite{zhaobjut}, we propose an approach which produces a temporal summary of an event in  three consecutive stages, and which is made up of the following modules: the information pre-processing module, the document retrieval and sentence extraction module, and finally the information summarization module. 

The \textit{Information Pre-processing Module} is necessary for converting the data into a ready to use format. Since the prefiltered TREC KBA Stream Corpus is a collection of encrypted file which cannot be used directly, the first step is to decrypt the files using the private key provided by the organizers. This will convert each .gpg file inside the corpus into a .sc file format. The next step is to retrieve the clean text from an .sc file, as it contains the web page with the entire HTML mark-up. To this end, the Stream Corpus toolbox\footnote{\url{https://github.com/trec-kba/streamcorpus}} made available by TREC provides tools for common data interchange format, which eventually converts an .sc file into a plain text .txt file.

The \textit{Document Retrieval and Sentence Extraction Module} is aimed at enhancing the retrieval of relevant sentences describing a query event. To this end, we build an index of the .txt files using ElasticSearch\footnote{\url{https://www.elastic.co/}}, an open source search analytics engine based on Apache Lucene which aims to provide scalable search solutions and research in language modeling and information retrieval. We fire \textit{more like this} queries to ElasticSearch using the query description of the event, and get back a set of relevant documents within the time range of the event.
Since we expect a high degree of vocabulary mismatch between the event description as specified in the query and the associated information nuggets for the respective event~\cite{mccreadie2014university}, we apply query expansion techniques though which we augment a query word with words having a similar meaning. We expect query expansion to increase the likelihood of matching words inside relevant documents, and therefore increase the number of relevant results returned.

For each document we can either assume that it is relevant for the event query, or further use a machine learning classifier to determine whether it is related to the event of interest or not. As training material we rely on the TREC Temporal Summarization test topics from past years (2013 and 2014), and extract features that reflect the degree of similarity between the document and the event representation as specified in the query. Other resources for expanding the query event rely upon the use of Freebase and DBpedia.  

Having identified the relevant documents for the query event, we proceed to sentence extraction from these documents. In extracting the sentences we need to account for the fact that sentences which are too long but partly relevant are doomed to either not appear in the final summary, or prevent inclusion of other relevant sentences\cite{martins2009summarization}. Additionally, sentences which are too short may not provide enough information, so we need to find a trade-off between sentence length and sentence quality and informativeness. One of the simplest methods we can apply consists in greedily extracting the leading sentences of each paragraph from documents while they fit into the summary. This strategy is known to perform well in newswire articles, due to the journalistic convention of summarizing the article first. Another strategy would be to rank sentences inside the relevant documents by their relevance score, and extract the top ones which fit into the summary. Features for ranking the sentences we can use the reciprocal position inside the document, a binary feature indicating whether the sentence is the first one or not, whether the sentence contains one or more named entities, the sentence length and the cosine similarity between the sentence and document title/entire document. Therefore the score for a sentence would be a linear function of feature values:

\begin{equation}
score_{rel}(t_i)=\sum_{d=1}^{D} \theta_d f_d(t_i),
\end{equation}

where $f_d(t_i)$ is a feature extracted from sentence $t_i$ and $\theta_d$ is its corresponding weight. Browsing window selections based on sliding windows can also be employed for selecting sentences nearby those which are estimated to be relevant.

The \textit{Information Summarisation Module} 

For large collections like the TREC KBA data we are using, it becomes important to penalize redundancy among the extracted sentences, and issue only sentences which contain novel information as updates. Typical ways to do so is to use the greedy cosine similarity heuristic in order to remove sentence which are overly similar to the ones already selected. Documents can be represented in the Vector Space Model where each word inside the document is weighted by its tf.idf score. The cosine distance or the mutual information preserving mapping metric can be used to compute the textual similarity between sentences. Unsupervised text clustering methods, such as k-means text clustering are another way to partition the data into clusters, and choose the cluster centers as the sentences to include inside the summarization. Finally, ranking sentences by time will yield the temporal summarisation output required for the task at hand.


